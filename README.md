# poe.ninja API client
A Java API client for [poe.ninja][poe-ninja], a [Path of Exile][path-of-exile] data aggregator.

An **overview** in the context of poe.ninja is a **league-specific, expiring snapshot** of data about a finite class of in-game items.

This library supports the following endpoints:
* [Statistics][statistics]
* [Currency Overview][currency] and [Currency History][currency-history]
* [Fragment Overview][fragment] and [Fragment History][fragment-history]
* [Map Overview][map] and [Map History][map-history]
* [Watchstone Overview][watchstone] and [Watchstone History][watchstone-history]
* [DivinationCard Overview][divination-card] and [DivinationCard History][divination-card-history]
* [Prophecy Overview][prophecy] and [Prophecy History][prophecy-history]
* [Essence Overview][essence] and [Essence History][essence-history]
* [Beast Overview][beast] and [Beast History][beast-history]
* [Vial Overview][vial] and [Vial History][vial-history]
* [Fossil Overview][fossil] and [Fossil History][fossil-history]
* [Resonator Overview][resonator] and [Resonator History][resonator-history]
* [Scarab Overview][scarab] and [Scarab History][scarab-history]
* [Incubator Overview][incubator] and [Incubator History][incubator-history]
* [Oil Overview][oil] and [Oil History][oil-history]
* [DeliriumOrb Overview][delirium-orb] and [DeliriumOrb History][delirium-orb-history]
* [Invitation Overview][invitation] and [Invitation History][invitation-history]
* [SkillGem Overview][skill-gem] and [SkillGem History][skill-gem-history]
* [BaseType Overview][base-type] and [BaseType History][base-type-history]
* [UniqueArmour Overview][unique-armour] and [UniqueArmour History][unique-armour-history]
* [UniqueWeapon Overview][unique-weapon] and [UniqueWeapon History][unique-weapon-history]
* [UniqueAccessory Overview][unique-accessory] and [UniqueAccessory History][unique-accessory-history]
* [UniqueJewel Overview][unique-jewel] and [UniqueJewel History][unique-jewel-history]
* [UniqueFlask Overview][unique-flask] and [UniqueFlask History][unique-flask-history]
* [UniqueMap Overview][unique-map] and [UniqueMap History][unique-map-history]
* [HelmetEnchant Overview][helmet-enchant] and [HelmetEnchant History][helmet-enchant-history]
* [Rules][rules]
* [Build Overview by XP][build-overview-by-exp]
* [Build Overview by Delve solo depth][build-overview-by-depth-solo]

## Data expiry
If you'd like to obtain a fresh copy of data every time you make a request to the library, use:
```java
PoeNinjaClient client = new PoeNinjaClient();
```

If, in contrast, you'd like your data to refresh only when the `Expires` header of the data indicates the data is stale, use instead:
```java
PoeNinjaClient client = new CachedPoeNinjaClient();
```

## Usage
```java
PoeNinjaClient client = new CachedPoeNinjaClient("Standard");
CurrencyOverview overview = client.getCurrencyOverview();

for (Currency currency : overview.getLines()) {
    double chaosEquivalent = currency.getChaosEquivalent();
    // ...
}
```

All fields available on the API are available as Java fields of the same name.

## Build
```shell script
gradlew clean build
```

## Dependencies
* JDK 11 (`HttpClient`)
* [Jackson][jackson] (to deserialize API responses)

If you're looking to fork this library or develop it further, be aware it also uses:
* [Lombok][lombok]
* [JUnit 5][junit-5]

[poe-ninja]: https://poe.ninja/
[path-of-exile]: https://www.pathofexile.com/
[jackson]: https://github.com/FasterXML/jackson
[lombok]: https://projectlombok.org/
[junit-5]: https://junit.org/junit5/
[statistics]: https://poe.ninja/api/data/GetStats
[rules]: https://poe.ninja/api/data/GetRules
[currency]: https://poe.ninja/api/data/CurrencyOverview?league=Standard&type=Currency
[fragment]: https://poe.ninja/api/data/CurrencyOverview?league=Standard&type=Fragment
[map]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Map
[watchstone]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Watchstone
[divination-card]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=DivinationCard
[prophecy]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Prophecy
[essence]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Essence
[beast]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Beast
[vial]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Vial
[fossil]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Fossil
[resonator]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Resonator
[scarab]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Scarab
[incubator]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Incubator
[oil]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Oil
[delirium-orb]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=DeliriumOrb
[invitation]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=Invitation
[skill-gem]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=SkillGem
[base-type]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=BaseType
[unique-armour]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=UniqueArmour
[unique-weapon]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=UniqueWeapon
[unique-accessory]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=UniqueAccessory
[unique-jewel]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=UniqueJewel
[unique-flask]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=UniqueFlask
[unique-map]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=UniqueMap
[helmet-enchant]: https://poe.ninja/api/data/ItemOverview?league=Standard&type=HelmetEnchant
[currency-history]: https://poe.ninja/api/data/CurrencyHistory?league=Standard&type=Currency&currencyId=2
[fragment-history]: https://poe.ninja/api/data/CurrencyHistory?league=Standard&type=Fragment&currencyId=82
[map-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Map&itemId=23261
[watchstone-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Watchstone&itemId=36685
[divination-card-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=DivinationCard&itemId=6
[prophecy-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Prophecy&itemId=1
[essence-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Essence&itemId=369
[beast-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Beast&itemId=5111
[vial-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Vial&itemId=39818
[fossil-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Fossil&itemId=7634
[resonator-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Resonator&itemId=7635
[scarab-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Scarab&itemId=18966
[incubator-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Incubator&itemId=21674
[oil-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Oil&itemId=22523
[delirium-orb-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=DeliriumOrb&itemId=40903
[invitation-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=Invitation&itemId=55748
[skill-gem-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=SkillGem&itemId=2598
[base-type-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=BaseType&itemId=7837
[unique-armour-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=UniqueArmour&itemId=17
[unique-weapon-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=UniqueWeapon&itemId=12
[unique-accessory-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=UniqueAccessory&itemId=65
[unique-jewel-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=UniqueJewel&itemId=58
[unique-flask-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=UniqueFlask&itemId=125
[unique-map-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=UniqueMap&itemId=2245
[helmet-enchant-history]: https://poe.ninja/api/data/ItemHistory?league=Standard&type=HelmetEnchant&itemId=6035
[build-overview-by-exp]: https://poe.ninja/api/data/something/GetBuildOverview?overview=heist&type=exp
[build-overview-by-depth-solo]: https://poe.ninja/api/data/something/GetBuildOverview?overview=heist&type=depthsolo
