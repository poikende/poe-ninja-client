package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import uk.co.paulbenn.poeninjaclient.util.Expirable;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class ItemOverview extends Expirable {
    @JsonProperty("lines")
    private List<Item> lines;
    @JsonProperty("language")
    private Language language;
}
