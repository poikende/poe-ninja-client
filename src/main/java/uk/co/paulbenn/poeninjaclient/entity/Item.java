package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Item {
    @JsonProperty("id")
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("mapTier")
    private int mapTier;
    @JsonProperty("levelRequired")
    private int levelRequired;
    @JsonProperty("baseType")
    private String baseType;
    @JsonProperty("stackSize")
    private int stackSize;
    @JsonProperty("variant")
    private String variant;
    @JsonProperty("prophecyText")
    private String prophecyText;
    @JsonProperty("artFilename")
    private String artFilename;
    @JsonProperty("links")
    private int links;
    @JsonProperty("itemClass")
    private int itemClass;
    @JsonProperty("sparkline")
    private SparkLine sparkline;
    @JsonProperty("lowConfidenceSparkline")
    private SparkLine lowConfidenceSparkline;
    @JsonProperty("implicitModifiers")
    private List<ItemModifier> implicitModifiers;
    @JsonProperty("explicitModifiers")
    private List<ItemModifier> explicitModifiers;
    @JsonProperty("flavourText")
    private String flavourText;
    @JsonProperty("corrupted")
    private boolean corrupted;
    @JsonProperty("gemLevel")
    private int gemLevel;
    @JsonProperty("gemQuality")
    private int gemQuality;
    @JsonProperty("itemType")
    private String itemType;
    @JsonProperty("chaosValue")
    private double chaosValue;
    @JsonProperty("exaltedValue")
    private double exaltedValue;
    @JsonProperty("count")
    private int count;
    @JsonProperty("detailsId")
    private String detailsId;
    @JsonProperty("tradeInfo")
    private TradeInfo tradeInfo;
    @JsonProperty("mapRegion")
    private String mapRegion;
    @JsonProperty("listingCount")
    private int listingCount;
}
