package uk.co.paulbenn.poeninjaclient.entity.build;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import uk.co.paulbenn.poeninjaclient.deserializer.WrappedStringDeserializer;

import java.util.List;
import java.util.Map;

@Data
public class SupportGems {
    @JsonProperty("names")
    @JsonDeserialize(contentUsing = WrappedStringDeserializer.class)
    private List<String> names;
    @JsonProperty("use")
    private Map<Integer, List<Integer>> use;
    @JsonProperty("dictionary")
    private Map<String, Integer> dictionary;
}
