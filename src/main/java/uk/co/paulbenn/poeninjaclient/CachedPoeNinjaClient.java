package uk.co.paulbenn.poeninjaclient;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import uk.co.paulbenn.poeninjaclient.entity.CurrencyHistory;
import uk.co.paulbenn.poeninjaclient.entity.CurrencyOverview;
import uk.co.paulbenn.poeninjaclient.entity.ItemHistory;
import uk.co.paulbenn.poeninjaclient.entity.ItemOverview;
import uk.co.paulbenn.poeninjaclient.entity.build.BuildOverview;
import uk.co.paulbenn.poeninjaclient.model.SortType;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

// TODO a better way to do this
public class CachedPoeNinjaClient extends PoeNinjaClient {

    private final Map<String, CurrencyOverview> currencyOverviewCache;

    private final Map<String, ItemOverview> itemOverviewCache;

    private final Map<HistoryCacheKey, CurrencyHistory> currencyHistoryCache;

    private final Map<HistoryCacheKey, ItemHistory> itemHistoryCache;

    private final Map<SortType, BuildOverview> buildOverviewCache;

    public CachedPoeNinjaClient(String league) {
        super(league);
        this.currencyOverviewCache = new HashMap<>();
        this.itemOverviewCache = new HashMap<>();
        this.currencyHistoryCache = new HashMap<>();
        this.itemHistoryCache = new HashMap<>();
        this.buildOverviewCache = new HashMap<>();
    }

    @Override
    protected CurrencyOverview getCurrencyOverview(String type) {
        return currencyOverviewCache.compute(
            type,
            (key, value) -> {
                if (value == null || value.getExpiresAt().isAfter(Instant.now())) {
                    return super.getCurrencyOverview(type);
                }
                return value;
            }
        );
    }

    @Override
    protected ItemOverview getItemOverview(String type) {
        return itemOverviewCache.compute(
            type,
            (key, value) -> {
                if (value == null || value.getExpiresAt().isAfter(Instant.now())) {
                    return super.getItemOverview(type);
                }
                return value;
            }
        );
    }

    @Override
    protected CurrencyHistory getCurrencyHistory(@NonNull String type, @NonNull String currencyId) {
        return currencyHistoryCache.compute(
            new HistoryCacheKey(type, currencyId),
            (key, value) -> {
                if (value == null || value.getExpiresAt().isAfter(Instant.now())) {
                    return super.getCurrencyHistory(type, currencyId);
                }
                return value;
            }
        );
    }

    @Override
    protected ItemHistory getItemHistory(@NonNull String type, @NonNull String itemId) {
        return itemHistoryCache.compute(
            new HistoryCacheKey(type, itemId),
            (key, value) -> {
                if (value == null || value.getExpiresAt().isAfter(Instant.now())) {
                    return super.getItemHistory(type, itemId);
                }
                return value;
            }
        );
    }

    public BuildOverview getBuildOverview(SortType sortType) {
        return buildOverviewCache.compute(
            sortType,
            (key, value) -> {
                if (value == null || value.getExpiresAt().isAfter(Instant.now())) {
                    return super.getBuildOverview(sortType);
                }
                return value;
            }
        );
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    private static class HistoryCacheKey {
        private final String type;
        private final String id;
    }
}
