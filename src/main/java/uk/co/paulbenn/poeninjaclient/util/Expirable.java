package uk.co.paulbenn.poeninjaclient.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public abstract class Expirable {
    @JsonIgnore
    private Instant expiresAt;
}
