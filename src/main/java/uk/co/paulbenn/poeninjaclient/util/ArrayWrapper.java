package uk.co.paulbenn.poeninjaclient.util;

public interface ArrayWrapper<E> {
    void setValues(E[] array);
}
