package uk.co.paulbenn.poeninjaclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import uk.co.paulbenn.poeninjaclient.util.ArrayWrapper;
import uk.co.paulbenn.poeninjaclient.util.Expirable;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class DeserializingHttpClient {

    private final HttpClient httpClient;

    private final ObjectMapper objectMapper;

    public DeserializingHttpClient() {
        this.httpClient = HttpClient.newHttpClient();
        this.objectMapper = JsonMapper.builder().findAndAddModules().build();
    }

    public <T> T parse(URI uri, Class<T> valueType) {
        try {
            HttpResponse<InputStream> response = openInputStream(uri);

            return objectMapper.readValue(response.body(), valueType);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public <T extends Expirable> T parseExpirable(URI uri, Class<T> expirableType) {
        try {
            HttpResponse<InputStream> response = openInputStream(uri);

            T value = objectMapper.readValue(response.body(), expirableType);
            value.setExpiresAt(getExpiryDate(response.headers()));

            return value;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    // deal with root-level arrays
    public <E, T extends Expirable & ArrayWrapper<E>> T parseExpirableArray(URI uri, T expirableType, Class<E> arrayElementType) {
        try {
            HttpResponse<InputStream> response = openInputStream(uri);

            Object value = objectMapper.readValue(
                response.body(),
                arrayElementType.arrayType()
            );

            @SuppressWarnings("unchecked")
            E[] values = (E[]) value;

            expirableType.setValues(values);
            expirableType.setExpiresAt(getExpiryDate(response.headers()));

            return expirableType;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private HttpResponse<InputStream> openInputStream(URI uri) {
        HttpRequest request = buildRequest(uri);

        try {
            return httpClient.send(request, HttpResponse.BodyHandlers.ofInputStream());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private HttpRequest buildRequest(URI uri) {
        return HttpRequest.newBuilder()
            .uri(uri)
            .header("User-Agent", "gitlab.com/PaulBenn/poe-ninja-client 1.0")
            .build();
    }

    private Instant getExpiryDate(HttpHeaders headers) {
        return headers.firstValue("expires")
            .map(headerValue -> LocalDateTime.parse(headerValue, DateTimeFormatter.RFC_1123_DATE_TIME))
            .map(expiresLocalDateTime -> expiresLocalDateTime.toInstant(ZoneOffset.UTC))
            .orElseGet(Instant::now);
    }
}
